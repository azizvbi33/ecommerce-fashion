@extends('layouts.master')

@section('title')
    Data kategori
@endsection

@section('content')
<br><br><br>
@auth
<a href="/kategori/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">Nomor</th>
                <th scope="col">Nama kategori</th>
                <th scope="col">Show</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($kategori as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->nama}}</td>
                        <td>
                            <a href="/kategori/{{$value->id}}" class="btn btn-info">Show</a>
                        </td>
                        <td>
                            @auth
                            <a href="/kategori/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            @endauth
                        </td>
                        <td>
                            @auth
                            <form action="/kategori/{{$value->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                            @endauth
                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection