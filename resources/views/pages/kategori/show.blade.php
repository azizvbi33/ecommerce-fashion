@extends('layouts.master')

@section('title')
    Tampilkan Detail produk
@endsection

@section('content')
<br><br><br>
<h1 class="text-info my-3">{{$kategori->nama}}</h1>

<div class="row">
  @forelse ($kategori->produk as $item)
    <div class="col-4">
      <div class="card" style="width: 18rem;">
        <img src="{{asset('images/produk/'.$item->image)}}" class="card-img-top" alt="...">
        <div class="card-body">
          <span>{{$item->kategori->nama}}</span>
            <h2>Show produk {{$item->id}}</h2>
          <h5 class="card-title">{{$item->judul}}</h5>
          <p class="card-text">{{$item->deskripsi}}</p>
          <a href="/produk/{{$item->id}}" class="btn btn-info">Readme</a>
        </div>
      </div>
    </div>
    @empty
        Tidak ada produk di kategori ini
    @endforelse
</div>
@endsection