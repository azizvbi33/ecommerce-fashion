@extends('layouts.master')
@section('content')
<br><br><br><br>
    <form method="POST" action="/profil/{{$profil->id}}" class="my-5">
        @method('PUT')
        @csrf
        <div class="row mb-3">
            <label class="col-md-2 col-form-label text-md-end">{{ __('Nama') }}</label>
    
            <div class="col-md-6">
                <input id="nama" type="text" class="form-control @error('nama') is-invalid @enderror" name="nama" value="{{$profil->nama}}" autocomplete="nama" autofocus>
    
                @error('nama')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    
        <div class="row mb-3">
            <label class="col-md-2 col-form-label text-md-end">{{ __('Tanggal Lahir') }}</label>
    
            <div class="col-md-6">
                <input id="tgl_lahir" type="date" class="form-control @error('tgl_lahir') is-invalid @enderror" name="tgl_lahir" value="{{ $profil->tgl_lahir}}" autocomplete="tgl_lahir" autofocus>
    
                @error('tgl_lahir')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-md-2 col-form-label text-md-end">{{ __('Alamat') }}</label>
    
            <div class="col-md-6">
                <input id="alamat" type="text" class="form-control @error('alamat') is-invalid @enderror" name="alamat" value="{{ $profil->alamat }}" autocomplete="alamat" autofocus>
    
                @error('alamat')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="row mb-3">
            <label class="col-md-2 col-form-label text-md-end">{{ __('Nomor Telfon') }}</label>
    
            <div class="col-md-6">
                <input id="no_telp" type="text" class="form-control @error('no_telp') is-invalid @enderror" name="no_telp" value="{{ $profil->no_telp }}" autocomplete="no_telp" autofocus>
    
                @error('no_telp')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    
    
        <div class="row mb-0">
            <div class="col-md-6 offset-md-2">
                <button type="submit" class="btn btn-primary">
                    {{ __('Edit Profile') }}
                </button>
            </div>
        </div>
    </form>
@endsection