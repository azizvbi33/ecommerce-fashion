@extends('layouts.master')
@section('content')
	<!-- Slider -->
	@include('partials.slider')

	<!-- Banner -->
	@include('partials.banner')


	<!-- Product -->
	@include('partials.product')
@endsection