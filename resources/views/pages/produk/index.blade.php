@extends('layouts.master')

@section('title')
    Data produk
@endsection

@section('content')
<br><br><br>
@auth
<a href="/produk/create" class="btn btn-primary mb-3">Tambah</a>
@endauth
        <table class="table">
            <thead class="thead-light">
              <tr>
                <th scope="col">#</th>
                <th scope="col">Judul</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Nama Kategori</th>
                <th scope="col">Image</th>
                <th scope="col">Actions</th>
              </tr>
            </thead>
            <tbody>
                @forelse ($produk as $key=>$value)
                    <tr>
                        <td>{{$key + 1}}</th>
                        <td>{{$value->judul}}</td>
                        <td>{{ Str::limit($value->deskripsi, 10)}}</td>
                        <td>{{$value->kategori->nama}}</td>
                        <td><img src="{{asset('images/produk/'.$value->image)}}" alt="" width="100px"></td>
                        <td>
                            <a href="/produk/{{$value->id}}" class="btn btn-info">Show</a>
                            
                            @auth
                            <a href="/produk/{{$value->id}}/edit" class="btn btn-primary">Edit</a>
                            <form action="/produk/{{$value->id}}" method="post">
                                @csrf
                                @method('DELETE')
                                <input type="submit" class="btn btn-danger my-1" value="Delete">
                            </form>
                            @endauth

                        </td>
                    </tr>
                @empty
                    <tr colspan="3">
                        <td>No data</td>
                    </tr>  
                @endforelse              
            </tbody>
        </table>
@endsection