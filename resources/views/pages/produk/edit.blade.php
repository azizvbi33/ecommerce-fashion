
@extends('layouts.master')

@section('judul')
    Data produk
@endsection

@section('content')
<br><br><br>
    <div>
        <h2>Edit produk {{$produk->id}}</h2>
        <form action="/produk/{{$produk->id}}" method="Post" enctype="multipart/form-data">
            @csrf
            @method('PUT')
            <div class="form-group">
                <label for="judul">judul</label>
                <input type="text" class="form-control" name="judul" value="{{$produk->judul}}" id="judul" placeholder="Masukkan judul">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">deskripsi</label>
                <input type="text" class="form-control" name="deskripsi"  value="{{$produk->deskripsi}}"  id="deskripsi" placeholder="Masukkan deskripsi">
                @error('deskripsi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">Harga</label>
                <input type="number" class="form-control" name="harga" value="{{$produk->harga}}" id="deskripsi" placeholder="Masukkan deskripsi">
                @error('harga')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">image</label>
                <input type="file" class="form-control" name="image" value="{{asset('images/produk/'.$produk->image)}}" id="deskripsi" placeholder="Masukkan deskripsi">
                @error('image')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="deskripsi">kategori Id</label>
                <select name="kategori_id" id="" class="form-control">
                    <option value="">-- Pilih kategori Id Kamu --</option>
                    @forelse ($kategori as $item)
                        @if ($item->id == $produk->kategori_id)
                            <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                        @else 
                            <option value="{{$item->id}}">{{$item->nama}}</option>
                        @endif
                        @empty
                        <option value="">No Data</option>
                    @endforelse
                </select>
        
                @error('kategori_id')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Edit</button>
        </form>
    </div>
@endsection


