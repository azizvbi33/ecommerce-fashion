@extends('layouts.master')

@section('title')
    Silahkan Buat Data Baru
@endsection

@section('content')
<br><br><br>
<h2 class="my-3">Tambah Data Produk</h2>

<form action="/produk" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label >Judul</label>
        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan judul">
        @error('judul')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="deskripsi">Deskripsi</label>
        <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Masukkan deskripsi">
        @error('deskripsi')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="deskripsi">Harga</label>
        <input type="number" class="form-control" name="harga" id="deskripsi" placeholder="Masukkan deskripsi">
        @error('harga')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>

    <div class="form-group">
        <label for="deskripsi">Image</label>
        <input type="file" class="form-control" name="image" id="deskripsi" placeholder="Masukkan deskripsi">
        @error('image')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <div class="form-group">
        <label for="deskripsi">User Id</label>
        <select name="kategori_id" id="" class="form-control">
            <option value="">-- Pilih Kategori Kamu --</option>
            @forelse ($kategori as $item)
                <option value="{{$item->id}}">{{$item->nama}}</option>
                @empty
                <option value="">No Data</option>
            @endforelse
        </select>

        @error('kategori_id')
            <div class="alert alert-danger">
                {{ $message }}
            </div>
        @enderror
    </div>
    <button type="submit" class="btn btn-primary">Tambah</button>
</form>
@endsection