<?php
     use Darryldecode\Cart\Cart;
     $item = \Cart::getContent();
 ?>
<div class="container-menu-desktop">
    <!-- Topbar -->
    <div class="top-bar">
        <div class="content-topbar flex-sb-m h-full container">
            <div class="left-top-bar">
                Free shipping for standard order over $100
            </div>

            <div class="right-top-bar flex-w h-full">
                <a href="#" class="flex-c-m trans-04 p-lr-25">
                    Help & FAQs
                </a>
                <a href="/myaccount" class="flex-c-m trans-04 p-lr-25">
                    My Account
                </a>
                @guest
                <a href="/login" class="flex-c-m trans-04 p-lr-25 ">
                    Login
                </a>
                <a href="/register" class="flex-c-m trans-04 p-lr-25 ">
                    Register
                </a>
                 @endguest   
                @auth
                <a class="flex-c-m trans-04 p-lr-25 bg-danger " href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </a>
                @endauth
            </div>
        </div>
    </div>

    <div class="wrap-menu-desktop">
        <nav class="limiter-menu-desktop container">
            
            <!-- Logo desktop -->		
            <a href="/" class="logo">
                <img src="{{asset('/template/images/icons/logo-01.png')}}" alt="IMG-LOGO">
            </a>

            <!-- Menu desktop -->
            <div class="menu-desktop">
                <ul class="main-menu">
                    <li class="active-menu">
                        <a href="/">Home</a>
                        {{-- <ul class="sub-menu">
                            <li><a href="/">Homepage 1</a></li>
                            <li><a href="home-02.html">Homepage 2</a></li>
                            <li><a href="/home3">Homepage 3</a></li>
                        </ul> --}}
                    </li>
                    <li>
                        <a href="/product">Shop</a>
                    </li>
                    @auth
                    <li class="label1" data-label1="hot">
                        <a href="/keranjang">Check Out</a>
                    </li>
                    @endauth
                    @auth
                    <li>
                        <a href="/blog">Blog</a>
                    </li>
                    @endauth
                    <li>
                        <a href="/about">About</a>
                    </li>

                    <li>
                        <a href="/contact">Contact</a>
                    </li>
                </ul>
            </div>	
            {{-- js-show-cart --}}
            <!-- Icon header -->
            <div class="wrap-icon-header flex-w flex-r-m">
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-modal-search">
                    <i class="zmdi zmdi-search"></i>
                </div>

                <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti {{-- js-show-cart --}}" data-notify="{{$item->count()}}">
                    <a href="/keranjang" style="color: blue">
                        <i class="zmdi zmdi-shopping-cart"></i>
                    </a>
                </div>

                <a href="#" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti" data-notify="0">
                    <i class="zmdi zmdi-favorite-outline"></i>
                </a>
                {{-- <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 js-show-sidebar">
                    <i class="zmdi zmdi-menu"></i>
				</div> --}}
            </div>

        </nav>
    </div>	
</div>