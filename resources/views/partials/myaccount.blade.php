<br>
<div class="sidebar-content flex-w w-full p-lr-65 js-pscroll">
    <div class="container">
        <div class="row mb-3">
            <div class="col-md-8">
                <div class="d-flex flex-row border rounded">
                      <div class="col-md-3">
                        <img src="https://c1.staticflickr.com/3/2862/12328317524_18e52b5972_k.jpg" class="img-thumbnail border-0" />
                        @auth
                        <h4 class="text-info">{{ Auth::user()->profil->nama }}</h4>
                        <i class="fa fa-phone" aria-hidden="true"></i> {{ Auth::user()->profil->no_telp }}
                        @endauth
                        @guest
                        <h4 class="text-primary">Anda Belum Login</h4>
                        @endguest
                        <ul>
                            <li class="stext-108 cl6 p-t-5">
                                <i class="fa fa-facebook-square" aria-hidden="true"></i> Facebook
                            </li>
                            <li class="stext-108 cl6 p-t-5">
                                <i class="fa fa-twitter" aria-hidden="true"></i> Twitter
                            </li>
                        </ul>
                      </div>
                      <div class="col-md-3">
                              <ul>
                                <span class="mtext-101 cl5 text-info">
                                    Features
                                </span> 
                                  <li class="stext-108 cl6 p-t-5">
                                    <a href="/#" class="stext-102 cl2 hov-cl1 trans-04" >
                                        Home
                                    </a>
                                </li>
                                <li class="stext-108 cl6 p-t-5">
                                    <a href="#" class="stext-102 cl2 hov-cl1 trans-04">
                                        Track Oder
                                    </a>
                                </li>
                        
                                <li class="stext-108 cl6 p-t-5">
                                    <a href="#" class="stext-102 cl2 hov-cl1 trans-04">
                                        Refunds
                                    </a>
                                </li>
                        
                                <li class="stext-108 cl6 p-t-5">
                                    <a href="#" class="stext-102 cl2 hov-cl1 trans-04">
                                        Help & FAQs
                                    </a>
                                </li>
                              </ul>
                      </div>
                      <div class="col-md-3">
                        <ul>
                            <li>
                                <span class="mtext-101 cl5 text-info">
                                    About Us
                                </span>
                                <p class="stext-108 cl6 p-t-5">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur maximus vulputate hendrerit. Praesent faucibus erat vitae rutrum gravida. Vestibulum tempus mi enim, in molestie sem fermentum quis. 
                                </p>
                            </li>
                        </ul>
                      </div>
                      @auth
                      <div class="pl-3 pt-2 pr-2 pb-2 w-75 border-left">
                        <p class="text-right m-0"><a href="/profil" class="btn btn-primary"><i class="fa fa-user"></i> Edit Profile</a></p>
                     </div>
                      @endauth
                </div>
            </div>
        </div>
    </div>
    <div class="sidebar-gallery w-full p-tb-30">
        <span class="mtext-101 cl5">
            Fashion Store
        </span>

        <div class="flex-w flex-sb p-t-36 gallery-lb">
            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-01.jpg')}}" data-lightbox="gallery" 
                style="background-image: url({{asset('/template/images/gallery-01.jpg')}});"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-02.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-02.jpg')}}');"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-03.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-03.jpg')}}');"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-04.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-04.jpg')}}');"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-05.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-05.jpg')}}');"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-06.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-06.jpg')}}');"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-07.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-07.jpg')}}');"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-08.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-08.jpg')}}');"></a>
            </div>

            <!-- item gallery sidebar -->
            <div class="wrap-item-gallery m-b-10">
                <a class="item-gallery bg-img1" href="{{asset('/template/images/gallery-09.jpg')}}" data-lightbox="gallery" 
                style="background-image: url('{{asset('/template/images/gallery-09.jpg')}}');"></a>
            </div>
        </div>
    </div>
    </div>
</div>