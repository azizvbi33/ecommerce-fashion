<div class="menu-mobile">
    <ul class="topbar-mobile">
        <li>
            <div class="left-top-bar">
                Free shipping for standard order over $100
            </div>
        </li>

        <li>
            <div class="right-top-bar flex-w h-full">
                {{-- <a href="#" class="flex-c-m p-lr-10 trans-04">
                    Help & FAQs
                </a> --}}

                <a href="/myaccount" class="flex-c-m p-lr-10 trans-04">
                    My Account
                </a>

                @guest
                <a href="/login" class="flex-c-m trans-04 p-lr-25 ">
                    Login
                </a>
                <a href="/register" class="flex-c-m trans-04 p-lr-25 ">
                    Register
                </a>
                 @endguest 
                 @auth
                <a class="flex-c-m trans-04 p-lr-25 bg-danger " href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </a>
                @endauth
            </div>
        </li>
    </ul>

    <ul class="main-menu-m">
        <li>
            <a href="/">Home</a>
            <span class="arrow-main-menu-m">
                <i class="fa fa-angle-right" aria-hidden="true"></i>
            </span>
        </li>

        <li>
            <a href="/product">Shop</a>
        </li>

        <li>
            <a href="/keranjang" class="label1 rs1" data-label1="hot">Features</a>
        </li>

        <li>
            <a href="/blog">Blog</a>
        </li>

        <li>
            <a href="/about">About</a>
        </li>

        <li>
            <a href="/contact">Contact</a>
        </li>
    </ul>
</div>