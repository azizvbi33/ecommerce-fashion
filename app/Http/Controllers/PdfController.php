<?php

namespace App\Http\Controllers;

use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class PdfController extends Controller
{
    public function invoice()
    {
        $data ="Data Invoice";
        $pdf = PDF::loadView('pdf.invoice', compact('data'));
        return $pdf->stream('invoice.pdf');

    }
}
