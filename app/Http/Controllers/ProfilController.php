<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Profil;
use Illuminate\Support\Facades\Auth;
 
class ProfilController extends Controller
{
    public function index()
    {
        
        // Retrieve the currently authenticated user's ID...
        $idsuer = Auth::id();
        $profil = Profil::where('user_id', $idsuer)->first();
        return view('pages.profil.index', ['profil' => $profil]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => ['required', 'string', 'max:255'],
            'tgl_lahir' => ['required', 'date'],
            'alamat' => ['required', 'string', 'max:255'],
            'no_telp' => ['required', 'string', 'max:255'],
        ]);
        $profil = Profil::find($id);
 
        $profil->nama = $request->nama;
        $profil->tgl_lahir = $request->tgl_lahir;
        $profil->alamat = $request->alamat;
        $profil->no_telp = $request->no_telp;
        
        $profil->save();

        return redirect('/profil');
    }
    // public function index()
    // {
    //     $profil = auth()->user()->profil;
    //     return view('pages.profil', compact('profil'));
    // }

    // public function update(Request $request)
    // {
    //     $profil = auth()->user()->profil;
    //     $profil->update($request->all());
    //     return redirect()->back();
    // }
}
