<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Keranjang;
use App\Models\Produk;
use App\Models\Pembelian;
use App\Models\User;

class KeranjangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $item = \Cart::getContent();
        $this->data['item'] = $item;

        return view('pages.keranjang.index', $this->data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $params = $request->except('_token');
        $produk = Produk::findOrFail($params['id_produk']);
        // $user = User::find($params['id_user']);
        $slug = $produk->slug;

        $attributes = [];
        if ($produk->configurable()) {
            $produk = Produk::from('produk as p')
                ->whereRaw("p.parent_id = {$params['id_produk']} AND p.jumlah >= {$params['kuantity']}")->firstOrFail();
            var_dump ($produk->id);
            $attributes = [
                'jumlah' => $params['kuantity'],
            ];
        }
        
        $request->validate([
            'kuantity' => 'required|min:1|max:255',
        ]);

        $item = [
            'id' => $produk->id,
            'name' => $produk->judul,
            'price' => $produk->harga,
            'quantity' => $params['kuantity'],
            'attributes' => $attributes,
            'associatedModel' => $produk,
        ];
        \Cart::add($item);
        \Session::flash('success', 'Produk'. $item['name'] . 'Produk berhasil ditambahkan ke keranjang');
        return redirect('/keranjang/'. $slug);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //update keranjang
        $params = $request->except('_token');
        $this->data['item'] = $item;

        if ($item = $params['item']) {
            foreach ($item as $id => $item) {
                \Cart::update($id, [
                    'quantity' => [
                        'relative' => false,
                        'value' => $item['kuantity']
                    ]
                ]);
            }
            \Session::flash('success', 'Keranjang berhasil diperbarui');
            return redirect('/keranjang');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \Cart::remove($id);
        \Session::flash('success', 'Produk berhasil dihapus dari keranjang');
        return redirect('/keranjang');
    }
}
