<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Produk;
use Illuminate\Support\Facades\DB;
use File;


class ProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = Produk::all();
        return view('pages.produk.index', compact('produk'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        return view('pages.produk.create', ['kategori' => $kategori]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
    		'judul' => 'required',
    		'deskripsi' => 'required',
            'harga' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'kategori_id' => 'required'
    	]);

        $image = $request->file('image');
        $new_image = time().$image->getClientOriginalName();
        $image->move(public_path('images/produk/'), $new_image);
 
        produk::create([
    		'judul' => $request->judul,
            'deskripsi' => $request->deskripsi,
            'harga' => $request->harga,                    
            'image' => $new_image,
            'kategori_id' => $request->kategori_id,
    	]);
 
    	return redirect('/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produk = Produk::find($id);
        return view('pages.produk.show', compact('produk'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produk = Produk::find($id);
        $kategori = Kategori::get();

        return view('pages.produk.edit', ['produk' => $produk, 'kategori' => $kategori]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {
        $request->validate([
            'judul' => 'required',
            'deskripsi' => 'required',
            'harga' => 'required',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'kategori_id' => 'required'
        ]);

        $produk = Produk::find($id);

        if($request->hasFile('image')){
            $destination = 'images/produk/'.$produk->image;
            if(File::exists($destination)){
                File::delete($destination);
            }
            $image = $request->file('image');
            $new_image = time().$image->getClientOriginalName();
            $image->move(public_path('images/produk/'), $new_image);
            $produk->image = $new_image;
        }

        $produk->judul = $request->judul;
        $produk->deskripsi = $request->deskripsi;
        $produk->harga = $request->harga;
        $produk->kategori_id = $request->kategori_id;
        $produk->update();
        return redirect('/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
        public function destroy($id)
        {
            $produk = Produk::find($id);
            $path = 'images/produk/';
            File::delete($path.$produk->image);
            $produk->delete();
            return redirect('/produk');
        }
}
