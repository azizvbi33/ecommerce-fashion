<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Keranjang extends Model
{
    use HasFactory;

    protected $table = 'keranjang';
    protected $fillable = ['confirmed', 'user_id', ];

    /**
     * Relational Function
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id')->withDefault();
    }

    public function pembelian()
    {
        return $this->hasMany(Pembelian::class, 'keranjang_id')->withDefault();
    }
}
