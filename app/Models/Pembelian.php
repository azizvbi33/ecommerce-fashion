<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Psy\CodeCleaner\ReturnTypePass;

class Pembelian extends Model
{
    use HasFactory;

    protected $table = 'pembelian';
    protected $fillable = ['kuantity', 'produk_id', 'keranjang_id', ];

    /**
     * Relational Function
     */
    public function keranjang()
    {
        return $this->belongsTo(Keranjang::class, 'keranjang_id')->withDefault();
    }

    public function produk()
    {
        return $this->belongsTo(Produk::class, 'produk_id')->withDefault();
    }
}
