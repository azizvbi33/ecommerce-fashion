<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    use HasFactory;

    protected $table = 'produk';
    protected $fillable = ['judul', 'deskripsi', 'harga', 'image', 'kategori_id'];

    /**
     * Relational Function
     */
    public function kategori()
    {
        return $this->belongsTo(Kategori::class, 'kategori_id')->withDefault();
    }

    public function pembelian()
    {
        return $this->hasMany(Pembelian::class, 'produk_id');
    }

    public function keranjang()
    {
        return $this->hasMany(Keranjang::class, 'produk_id');
    }

    public function configurable()
    {
        return $this->type == 'configurable';
    }

    public function simple()
    {
        return $this->type == 'simple';
    }

}
