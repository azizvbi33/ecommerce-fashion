# Final Project
## Kelompok 5
## Anggota Kelompok
-   Abdul Aziz
-   Isro Hidayatulloh
---
## Tema Project
**E-Commerce Fashion**
- 3 Library : DOMPDF, SweetAlert, TinyMCE
- 4 CRUD : Product, Category, Profile, Keranjang
---
## Link Video
## Link Demo: [Demo Fashion Ecommerce](https://youtu.be/z3oWbAO69jw)

## Link Deploy: [https://abdulaziz.sanbercodeapp.com/](https://abdulaziz.sanbercodeapp.com/)
## ERD
![](/public/images/erd.png)
---
