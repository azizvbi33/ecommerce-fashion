<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\ProdukController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\KeranjangController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth::routes();
Route::middleware(['auth'])->group(function () {
    
    
    Route::get('/master', function () {
        return view('layouts.master');
    });
      
    Route::get('/blog-detail', function () {
        return view('pages.blog-detail');
    });
    
    Route::get('/product-detail', function () {
        return view('pages.product-detail');
    });

    // Route::get('/shoping-cart', function () {
    //     return view('pages.shoping-cart');
    // });

    //Update Profile
    Route::resource('/profil', ProfilController::class)->only([
        'index', 'update'
    ]);

    Route::resource('/kategori', KategoriController::class);
    Route::resource('/produk', ProdukController::class);
    Route::resource('/keranjang', KeranjangController::class);
    
});

Route::get('/', function () {
    return view('welcome');
});

Route::get('/myaccount', function () {
    return view('pages.myaccount');
});

Route::get('/product', function () {
    return view('pages.product');
});

Route::get('/about', function () {
    return view('pages.about');
});

Route::get('/blog', function () {
    return view('pages.blog');
});

Route::get('/contact', function () {
    return view('pages.contact');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/invoice-pdf', [App\Http\Controllers\PdfController::class, 'invoice']);
